package pisith.demo.restapi.services;

import org.springframework.stereotype.Service;
import pisith.demo.restapi.models.Blog;

import java.util.List;
import java.util.Optional;

@Service
public interface BlogService {
    List<Blog> findAll();
    Blog addNew(Blog blog);
    Optional<Blog> findById(int id);
    Blog update(int id, Blog blog);
}
