package pisith.demo.restapi.services.services_impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pisith.demo.restapi.models.Blog;
import pisith.demo.restapi.repositories.BlogRepository;
import pisith.demo.restapi.services.BlogService;

import java.util.List;
import java.util.Optional;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired private BlogRepository blogRepository;
    @Override
    public List<Blog> findAll() {
        return this.blogRepository.findAll();
    }

    @Override
    public Blog addNew(Blog blog) {
        String content = blog.getContent();
        String title = blog.getTitle();
        return this.blogRepository.save(new Blog(title,content));
    }

    @Override
    public Optional<Blog> findById(int id) {
        return this.blogRepository.findById(id);
    }

    @Override
    public Blog update(int id, Blog blog) {
        Optional<Blog> oldBlog = this.blogRepository.findById(id);
        Blog newBlog = oldBlog.get();
        newBlog.setTitle(blog.getTitle());
        newBlog.setContent(blog.getContent());
        return this.blogRepository.save(newBlog);
    }
}
