package pisith.demo.restapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pisith.demo.restapi.models.Blog;
import pisith.demo.restapi.services.BlogService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BlogController {
    @Autowired private BlogService blogService;

    @GetMapping("/")
    private String welcome(){return "Welcome to Blog API";}

    @GetMapping("/blogs")
    private List<Blog> findAll() {return this.blogService.findAll();}

    @PostMapping
    private Blog addNew(@RequestBody Blog blog) {
        return this.blogService.addNew(blog);
    }

    @GetMapping("/{id}")
    private Optional<Blog> findById(@PathVariable int id) {
        return this.blogService.findById(id);
    }

    @PutMapping("/{id}")
    private Blog update (@PathVariable int id, @RequestBody Blog blog) {
        return this.blogService.update(id, blog);
    }
}
