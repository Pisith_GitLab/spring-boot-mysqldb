package pisith.demo.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import pisith.demo.restapi.models.Blog;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Integer> { }
